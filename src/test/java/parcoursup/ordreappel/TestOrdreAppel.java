/* Copyright 2019 © Ministère de l'Enseignement Supérieur, de la Recherche et de l'Innovation,
    Hugo Gimbert (hugo.gimbert@enseignementsup.gouv.fr)
    Jean-Rémy Falleri-Vialard (falleri@labri.fr)

    This file is part of Algorithmes-de-parcoursup.

    Algorithmes-de-parcoursup is free software: you can redistribute it and/or modify
    it under the terms of the Affero GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Algorithmes-de-parcoursup is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Affero GNU General Public License for more details.

    You should have received a copy of the Affero GNU General Public License
    along with Algorithmes-de-parcoursup.  If not, see <http://www.gnu.org/licenses/>.

 */

package parcoursup.ordreappel;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Random;

public class TestOrdreAppel {
  
  @Test
  public void testExemplesStatiques() throws Exception {
    ExempleA1 exempleA1 = new ExempleA1();
    exempleA1.execute(false);

    ExempleA2 exempleA2 = new ExempleA2();
    exempleA2.execute(false);

    ExempleA3 exempleA3 = new ExempleA3();
    exempleA3.execute(false);

    ExempleA4 exempleA4 = new ExempleA4();
    exempleA4.execute(false);

    ExempleA5 exempleA5 = new ExempleA5();
    exempleA5.execute(false);

    ExempleA6 exempleA6 = new ExempleA6();
    exempleA6.execute(false);
  }

  @Test
  public void testExemplesFaux() throws Exception {
    ExempleFaux exempleFaux = new ExempleFaux();
    Assertions.assertThrows(RuntimeException.class, () -> {
      exempleFaux.execute(false);
    });
  }

  @Test
  public void test1000InstancesAleatoires() throws Exception {
    for (int i = 0; i < 1000; i++) {
      Random r = new Random();
      ExempleAleatoire e = new ExempleAleatoire(1 + r.nextInt(1000));
      e.execute(false);
    }
  }
}